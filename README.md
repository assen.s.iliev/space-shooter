# Space Shooter
Space Shooter is a game developped following the first few chapters of the Udemy course [The Ultimate Guide to Game Development with Unity (Official)](https://www.udemy.com/course/the-ultimate-guide-to-game-development-with-unity/) by Jonathan Weinberger.

The game design, features and assets are content that is copied from the course. The course itself is, however, constructed as a series of challenges where the student is invited to develop each feature by
themselves. Author's solution is then provided, to keep the student's progress accordingly.

In this respect, the approaches, scripts content and decisions made for the realization are mine own and, in many cases, they diviate significantly from what author's was proposing as solution. For the features themselves, I tried to keep strictly to what was assumed by the course, but in terms of implementation, I could say that they are around 90% my own.


## Main features