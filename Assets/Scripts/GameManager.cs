using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private bool _isGameOver;

    [SerializeField] private GameObject _explosion;

    private void Start()
    {

    }


    public GameObject Explosion
    {
        get => _explosion;
    }

    public void GameOver()
    {
        _isGameOver = true;
        StartCoroutine(RestartScene());
    }

    IEnumerator RestartScene()
    {
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.R));
        SpawnManager.CurrentNumberOfEnemies = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
