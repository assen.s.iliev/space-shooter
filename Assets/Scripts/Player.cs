﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Player : MonoBehaviour
{
    public GameObject singleShot;
    public GameObject trippleShot;

    public bool IsPlayerAlive { get => _isAlive; }
    

    private float _upperBound = 0.0f;
    private float _lowerBound = -3.8f;    
    private float _rightBound = 9.5f;
    private float _leftBound;

    [SerializeField] private float _speed;
    [SerializeField] private float _speedBoostFactor;
    public float moveSpeed;

    [SerializeField] private float _fireRate;
    private float _nextFire = 0;

    [SerializeField] private int _lives;

    [SerializeField] private Transform _laserContainer;
    [SerializeField] private GameObject playerShield;

    [SerializeField] private float _trippleShotActiveTime;
    [SerializeField] private float _speedPowerUpActiveTime;
    [SerializeField] private float _shieldPowerUpActiveTime;

    [SerializeField] private int _score = 0;

    private bool _isAlive = true;

    private bool _isTrippleShotActive = false;
    private bool _isSpeedPowerUpActive = false;
    private bool _isShieldPowerUpActive = false;

    private AudioSource _audiosource;

    [SerializeField] private AudioClip shootSound;

    private IEnumerator trippleShootCoroutine;
    private IEnumerator speedPowerUpCoroutine;

    public bool IsTrippleShotActive
    {
        get => _isTrippleShotActive;
        set
        {
            _isTrippleShotActive = value;
            if (value)
            {
                if (trippleShootCoroutine != null)
                    StopCoroutine(trippleShootCoroutine);

                trippleShootCoroutine = KeepTripleShotActive();
                StartCoroutine(trippleShootCoroutine);
                
            }   
        }
    }

    public bool IsShieldPowerUpActive
    {
        get => _isShieldPowerUpActive;
        set
        {
            _isShieldPowerUpActive = value;
            if (value)
                playerShield.SetActive(true);
            else
                playerShield.SetActive(false);
        }
    }

    public bool IsSpeedPowerUpActive
    {
        get => _isSpeedPowerUpActive;
        set
        {
            _isSpeedPowerUpActive = value;
            if (value)
            {
                if (speedPowerUpCoroutine != null)
                    StopCoroutine(speedPowerUpCoroutine);

                speedPowerUpCoroutine = KeepSpeedPowerUpActive();
                StartCoroutine(speedPowerUpCoroutine);

            }
        }
    }

    private UIManager _uiManager;

    void Start()
    {
         _leftBound = -_rightBound;
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _uiManager.UpdateLivesImage(_lives);
        this.transform.position = new Vector3(0, -3.5f, 0);

        _audiosource = this.GetComponent<AudioSource>();
        
    }

    void Update()
    {
        Move();

        if (Input.GetKeyDown(KeyCode.LeftAlt) && Time.time > _nextFire)
        {
            Shoot();
        }
    }

    private void Move()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");

        //float moveSpeed;

        if (this.IsSpeedPowerUpActive)
            moveSpeed = _speed * _speedBoostFactor;
        else
            moveSpeed = _speed;

        this.transform.Translate(new Vector3(horizontalInput, verticalInput, 0) * moveSpeed * Time.deltaTime);

        this.transform.position = 
            new Vector3(Mathf.Clamp(this.transform.position.x, _leftBound, _rightBound),
                        Mathf.Clamp(this.transform.position.y, _lowerBound, _upperBound),
                        0);
    }

    private void Shoot()
    {
        if (IsTrippleShotActive)
            Instantiate(trippleShot, this.transform.position, Quaternion.identity, _laserContainer);
        else
            Instantiate(singleShot, this.transform.position, Quaternion.identity, _laserContainer);

        _audiosource.clip = shootSound;
        if(_isTrippleShotActive)
        {
            _audiosource.pitch = 1.5f;
            _nextFire = Time.time + _fireRate / 1.5f;
        }
        else
        {
            _audiosource.pitch = 1f;
            _nextFire = Time.time + _fireRate;
        }
        _audiosource.Play();
    }

    public void TakeDamage(int damageValue)
    {
        _lives -= damageValue;
        _uiManager.UpdateLivesImage(_lives);

        if (_lives <= 0) Die();
    }

    private void Die()
    {
        _isAlive = false;
        Destroy(this.gameObject);
    }

    IEnumerator KeepTripleShotActive()
    {
        _trippleShotActiveTime = Random.Range(3, 8);
        yield return new WaitForSeconds(_trippleShotActiveTime);

        _isTrippleShotActive = false;
    }

    IEnumerator KeepSpeedPowerUpActive()
    {
        _speedPowerUpActiveTime = Random.Range(3, 8);
        yield return new WaitForSeconds(_speedPowerUpActiveTime);

        _isSpeedPowerUpActive = false;
    }
}
